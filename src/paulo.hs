--
-- Projecto CP 2015/16
--
-- O projecto consiste em desenvolver testes para o módulo Graph.hs
-- (para grafos orientados e não pesados).
-- Mais concretamente, o projecto consiste em 3 tarefas que são descritas abaixo.
-- O prazo para entrega é o dia 3 de Abril. Cada grupo deve enviar apenas
-- o módulo de testes (este módulo) por email para calculodeprogramas@gmail.com
-- O nome do ficheiro deve identificar os números dos 2 alunos do grupo (numero1_numero2.hs).
-- Certifiquem-se que o módulo de testes desenvolvido compila correctamente antes
-- de submeter. O módulo Graph.hs não deve ser alterado.
-- Os 2 alunos do grupo devem também indentificar-se nos comentários abaixo.
--
-- Aluno 1
-- Número:
-- Nome:
-- Curso:
--
-- Aluno 2
-- Número:
-- Nome:
-- Curso:
--


module Main where

import Graph
import Test.HUnit hiding (path)
import Test.QuickCheck
import Data.Set as Set

--
-- Teste unitário
--
-- Nodos para teste
aresta_unica::Edge Int
aresta_unica= Edge 1 1

aresta_umdois::Edge Int
aresta_umdois = Edge 1 2

-- grafos para teste 
grafo_unitario :: Graph Int
grafo_unitario = Graph {nodes = fromList [1],
                    edges = fromList [Edge 1 1]
}
grafo_unitario2::Graph Int
grafo_unitario2 = Graph {nodes = fromList [20],
                        edges = fromList [Edge 20 20]        
}

-- grafo unitario mas sem caminho 
grafo_unitarioSC::Graph Int
grafo_unitarioSC=Graph{nodes = fromList [1],
                       edges = fromList []
}
grafo_vazio::Graph Int
grafo_vazio = Graph{nodes = fromList[],
                edges = fromList[]
}
grafo_arestaNP::Graph Int
grafo_arestaNP= Graph { nodes = fromList [1,2],
                        edges = fromList [(Edge 1 2),(Edge 1 3)]
                    }
grafo_duploSA::Graph Int
grafo_duploSA = Graph { nodes = fromList [1,2],
                        edges = fromList []
}
grafo_vazioCA :: Graph Int
grafo_vazioCA = Graph { nodes = fromList [],
                        edges=fromList[Edge 1 2]
}
grafo_arestaPGC::Graph Int
grafo_arestaPGC = Graph { nodes = fromList [1,2],
                         edges = fromList [(Edge 1 2),(Edge 2 1)]
}

grafo_NPePGC::Graph Int
grafo_NPePGC = Graph { nodes = fromList [1,2],
                        edges = fromList [(Edge 1 2),(Edge 1 3), (Edge 2 1)]
}
grafo_unicoCiclo::Graph Int
grafo_unicoCiclo = Graph { nodes = fromList [1,2,3],
                        edges = fromList [(Edge 1 2),(Edge 2 3),(Edge 3 3)]
}
grafo_nenhumCiclo::Graph Int
grafo_nenhumCiclo = Graph { nodes = fromList [1,2,3],
                        edges = fromList [(Edge 1 2),(Edge 2 3),(Edge 1 3)]
}

-- florestas
floresta_doisp::Graph Int
floresta_doisp = Graph {nodes = fromList [1,2,3,4,5],
                       edges = fromList [(Edge 1 2),(Edge 2 3),(Edge 3 4)]
} 
floresta_dois::Graph Int
floresta_dois = Graph {nodes = fromList [1,2,3,4,5,6,7],
                       edges = fromList [(Edge 1 2),(Edge 2 3),(Edge 3 4), (Edge 5 6), (Edge 6 7)]
}
floresta_doisT::Graph Int
floresta_doisT = Graph {nodes = fromList [1,2,3,4,5,6,7],
                       edges = fromList [(Edge 2 1),(Edge 3 2),(Edge 4 3), (Edge 6 5), (Edge 7 6)]
}

floresta_tres::Graph Int
floresta_tres = Graph {nodes = fromList [1,2,3,4,5,6,7,8,9],
                       edges = fromList [(Edge 1 2),(Edge 2 3),(Edge 3 4), (Edge 5 6), (Edge 6 7),(Edge 8 9)]
} 


--Testes--
----------
----------
----------
----------



-- grafo G([],[]) é vazio?
teste_isEmpty::Test
teste_isEmpty =isEmpty(grafo_unitario) ~?= False

-- grafo G([1],[Edge 1 1]) é vazio?
teste_isEmpty2::Test
teste_isEmpty2 =isEmpty(Graph.empty) ~?= True

-- swap da aresta 1 1, ciclico
teste_swap::Test
teste_swap = swap(aresta_unica) ~?= Edge 1 1

-- swap da aresta 1 2, nao ciclico
teste_swap2::Test
teste_swap2 = swap(aresta_umdois) ~?= Edge 2 1

-- teste isValid para grafo vazio
teste_isValid::Test
teste_isValid=isValid(grafo_vazio) ~?= True

-- teste isValid para grafo unitario
teste_isValid2::Test
teste_isValid2=isValid(grafo_unitario) ~?= True

-- teste isValid para grafo com aresta que nao pertencem ao grafo
teste_isValid3::Test
teste_isValid3=isValid(grafo_arestaNP) ~?= False

-- teste isValid para grafo com todas arestas pertencentes ao grafo e ciclico
teste_isValid4::Test
teste_isValid4=isValid(grafo_arestaPGC) ~?= True

-- teste isValid para grafo com um nodo mas sem nenhuma aresta
teste_isValid5::Test
teste_isValid5=isValid(grafo_unitarioSC) ~?= True

-- teste isDag para grafo vazio

teste_isDag::Test
teste_isDag=isDAG(grafo_vazio) ~?= True

-- teste isDag para grafo unitario e sem aresta
teste_isDag2::Test
teste_isDag2=isDAG(grafo_unitarioSC) ~?= True

-- teste isDag para grafo unitario e com aresta,ciclico
teste_isDag3::Test
teste_isDag3=isDAG(grafo_unitario) ~?= False

-- teste isDag para grafo com mais de um nodo e um unico ciclo
teste_isDag4::Test
teste_isDag4=isDAG(grafo_unicoCiclo) ~?= False

-- teste isDag para grafo com mais de um nodo completamente ciclico
teste_isDag5::Test
teste_isDag5=isDAG(grafo_arestaPGC) ~?= False

-- teste isDag para grafo com mais de um nodo e sem nenhuma aresta ciclica
teste_isDag6::Test
teste_isDag6=isDAG(grafo_nenhumCiclo) ~?= True

--teste isDag para uma floresta
teste_isDag7::Test
teste_isDag7 = isDAG(floresta_dois) ~?= True

--teste isForest para um grafo ciclico
teste_isForest::Test
teste_isForest = isForest(grafo_arestaPGC) ~?= False

--teste isForest para uma floresta

teste_isForest2::Test
teste_isForest2 = isForest(floresta_tres) ~?= True

-- teste isSubgraphOf para grafos sem nenhum nodo em comum

teste_isSubGraphOf::Test
teste_isSubGraphOf = isSubgraphOf grafo_unitario2 grafo_nenhumCiclo ~?= False

-- teste isSubgraphOF para grafos com pelo menos um  nodo em comum
teste_isSubGraphOf2::Test
teste_isSubGraphOf2 = isSubgraphOf grafo_arestaNP grafo_nenhumCiclo ~?= True

--teste isSubGraphOF para dois grafos iguais
teste_isSubGraphOf3::Test
teste_isSubGraphOf3 = isSubgraphOf grafo_nenhumCiclo grafo_nenhumCiclo ~?= True

-- teste adj para grafo sem arestas
teste_adj::Test
teste_adj = adj grafo_unitarioSC 1 ~?= fromList []

-- teste adj para grafo com arestas
teste_adj2::Test
teste_adj2 = adj grafo_unitario 1 ~?= fromList [Edge 1 1]

-- teste transpose para uma floresta
teste_transpose::Test
teste_transpose = transpose(floresta_dois) ~?= floresta_doisT

-- teste transpose para grafo sem aresta 
teste_transpose2::Test
teste_transpose2 = transpose(grafo_unitarioSC) ~?= grafo_unitarioSC
-- teste union grafos vazios
teste_union::Test
teste_union = Graph.union grafo_vazio grafo_vazio ~?= grafo_vazio

-- teste union para dois grafos n vazios
teste_union2::Test
teste_union2 = Graph.union grafo_arestaPGC grafo_arestaNP ~?= grafo_NPePGC

--teste bft com set n vazio
teste_bft::Test
teste_bft = bft floresta_dois (fromList[1,5]) ~?= transpose(floresta_dois)

--teste bft para set vazio
teste_bft2::Test
teste_bft2 = bft floresta_dois Set.empty ~=? Graph.empty

--teste isPath
teste_isPath::Test
teste_isPath = isPathOf [(Edge 1 2),(Edge 2 1)] grafo_arestaPGC ~?= True

--teste isPath sem um caminho valido

teste_isPath2::Test
teste_isPath2 = isPathOf [(Edge 1 2)] grafo_unitario2 ~?= False

-- teste_isPath para caminho vazio
teste_isPath3::Test
teste_isPath3 = isPathOf [] grafo_unitario2 ~?= True

-- teste path para vertices que n pertencem a um grafo
teste_path::Test
teste_path = path floresta_dois 8 1 ~?=  Nothing

-- teste path para vertices de um grafo
teste_path2::Test
teste_path2 = path floresta_dois 1 4 ~?= Just [(Edge 1 2),(Edge 2 3),(Edge 3 4)]

-- teste topo para uma floresta
teste_topo::Test
teste_topo = topo floresta_dois ~?= [fromList[1,5],fromList[2,6],fromList[3,7],fromList[4]]

teste_show::Test
teste_show = show grafo_vazio ~?= "Graph {nodes = fromList [], edges = fromList []}"

teste_show2::Test
teste_show2 = show grafo_duploSA ~?= "Graph {nodes = fromList [1,2], edges = fromList []}"

teste_show3::Test
teste_show3 = show grafo_vazioCA ~?= "Graph {nodes = fromList [], edges = fromList [Edge {source = 1, target = 2}]}"


teste_show4::Test
teste_show4 = show grafo_arestaNP ~?= "Graph {nodes = fromList [1,2], edges = fromList [Edge {source = 1, target = 2},Edge {source = 1, target = 3}]}"


teste_eqt::Test
teste_eqt = (Edge 1 2) == (Edge 1 2) ~?= True

teste_eqf::Test
teste_eqf = (Edge 2 1) /= (Edge 1 2) ~?= True

teste_eqt2::Test
teste_eqt2 = (Edge 2 1) == (Edge 3 1) ~?= False
-- Tarefa 1
--
-- Defina testes unitários para todas as funções do módulo Graph,
-- tentando obter o máximo de cobertura de expressões, condições, etc.
--        
main = runTestTT $ TestList [teste_isEmpty,teste_isEmpty2,
                            teste_swap,teste_swap2,
                            teste_isValid,teste_isEmpty2,teste_isValid3,teste_isValid4,teste_isValid5,
                            teste_isDag,teste_isDag2,teste_isDag3,teste_isDag4,teste_isDag5,teste_isDag6,teste_isDag7,
                            teste_isForest,teste_isForest2,
                            teste_isSubGraphOf,teste_isSubGraphOf2,teste_isSubGraphOf3,
                            teste_adj,teste_adj2,
                            teste_transpose,teste_transpose2,
                            teste_union,teste_union2,
                            teste_bft,teste_bft2,
                            teste_isPath,teste_isPath2,teste_isPath3,
                            teste_path,teste_path2,
                            teste_topo,
                            teste_show,teste_show2,teste_show3,teste_show4,
                            teste_eqt,teste_eqf,teste_eqt2]

--
-- Teste aleatório
--

--
-- Tarefa 2
--
-- A instância de Arbitrary para grafos definida abaixo gera grafos
-- com muito poucas arestas, como se pode constatar testando a
-- propriedade prop_valid.
-- Defina uma instância de Arbitrary menos enviesada.
-- Este problema ainda é mais grave nos geradores dag e forest que
-- têm como objectivo gerar, respectivamente, grafos que satisfazem
-- os predicados isDag e isForest. Estes geradores serão necessários
-- para testar propriedades sobre estas classes de grafos.
-- Melhore a implementação destes geradores por forma a serem menos enviesados.
--

-- Instância de Arbitrary para arestas
instance Arbitrary v => Arbitrary (Edge v) where
    arbitrary = do s <- arbitrary
                   t <- arbitrary
                   return $ Edge {source = s, target = t}

instance (Ord v, Arbitrary v) => Arbitrary (Graph v) where
    arbitrary = aux `suchThat` isValid
        where aux = do kns <- choose(0, 50)
                       ns  <- sequence (replicate kns arbitrary)
                       kes <- choose(0, 30)
                       es  <- edgeFromNodes kes ns
                       return $ Graph {nodes = fromList ns, edges = fromList es}

edgeFromNodes :: Int -> [a] -> Gen [(Edge a)]
edgeFromNodes _ [] = return []
edgeFromNodes 0 _ = return []
edgeFromNodes n l@(x:xs) = do origem <- elements l
                              destino <- elements l
                              mais <- edgeFromNodes (n-1) l
                              return $ ((Edge origem destino):mais)

 
prop_valid :: Graph Int -> Property
prop_valid g = collect (length (edges g)) $ isValid g

-- Gerador de DAGs
dag :: (Ord v, Arbitrary v) => Gen (DAG v)
dag = arbitrary `suchThat` isDAG

prop_dag :: Property
prop_dag = forAll (dag :: Gen (DAG Int)) $ \g -> collect (length (edges g)) $ isDAG g

-- Gerador de florestas
forest :: (Ord v, Arbitrary v) => Gen (Forest v)
forest = arbitrary `suchThat` isForest

prop_forest :: Property
prop_forest = forAll (forest :: Gen (Forest Int)) $ \g -> collect (length (edges g)) $ isForest g

--
-- Tarefa 3
--
-- Defina propriedades QuickCheck para testar todas as funções
-- do módulo Graph.
--

-- Exemplo de uma propriedade QuickCheck para testar a função adj          
prop_adj :: Graph Int -> Property
prop_adj g = forAll (elements $ elems $ nodes g) $ \v -> adj g v `isSubsetOf` edges g
