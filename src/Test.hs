--
-- Projecto CP 2015/16
--
-- O projecto consiste em desenvolver testes para o módulo Graph.hs
-- (para grafos orientados e não pesados).
-- Mais concretamente, o projecto consiste em 3 tarefas que são descritas abaixo.
-- O prazo para entrega é o dia 3 de Abril. Cada grupo deve enviar apenas
-- o módulo de testes (este módulo) por email para calculodeprogramas@gmail.com
-- O nome do ficheiro deve identificar os números dos 2 alunos do grupo (numero1_numero2.hs).
-- Certifiquem-se que o módulo de testes desenvolvido compila correctamente antes
-- de submeter. O módulo Graph.hs não deve ser alterado.
-- Os 2 alunos do grupo devem também indentificar-se nos comentários abaixo.
--
-- Aluno 1
-- Número: 61778
-- Nome: André Santos
-- Curso: Mestrado Integrado Engenharia Informática
--
-- Aluno 2
-- Número: 64282
-- Nome: Paulo Alves
-- Curso: Mestrado Integrado Engenharia Informática
--

module Main where

import Graph
import Test.HUnit hiding (path)
import Test.QuickCheck
import Data.Set as Set
import Data.Maybe

--
-- Tarefa 1 - Teste unitário
--
-- Defina testes unitários para todas as funções do módulo Graph,
-- tentando obter o máximo de cobertura de expressões, condições, etc.
--


main = runTestTT $ TestList [t_swap,
                                 t_isEmpty1, t_isEmpty2,
                                 t_isValid1, t_isValid2, t_isValid3,
                                 t_isDAG1, t_isDAG2,
                                 t_isForest1, t_isForest2,
                                 t_isSubGraphOf1, t_isSubGraphOf2,
                                 t_adj,
                                 t_transpose,
                                 t_union,
                                 t_bft1, t_bft2,t_bft3,
                                 t_reachable,
                                 t_isPathOf1, t_isPathOf2, t_isPathOf3,
                                 t_path1, t_path2,
                                 t_topo,
                                 t_EqEdge1, t_EqEdge2,
                                 t_showEdge,
                                 t_showGraph1, t_showGraph2, t_showGraph3, t_showGraph4
                                 ]


-- DEFINICAO GRAFOS E ARESTAS --

-- g_floresta
-- Grafo correspondente a uma floresta com duas árvores.
--        1     2
--         \   /
--          v v
--           3      5 -> 6
--           |
--           v
--           4

g_floresta :: Graph Int
g_floresta = Graph {
                    nodes = fromList [1,2,3,4,5,6],
                    edges = fromList [(Edge 1 3), (Edge 2 3), (Edge 3 4),(Edge 5 6)]
                    }

g_no :: Graph Int
g_no = Graph {
                    nodes = fromList [1,2],
                    edges = fromList [(Edge 1 2), (Edge 2 1)]
                    }

-- g_subArvore
-- Grafo correspondente a uma árvore. Sub-grafo de g_floresta
--        1     2
--         \   /
--          v v
--           3
--           |
--           v
--           4
g_subArvore :: Graph Int
g_subArvore = Graph {
                    nodes = fromList [1,2,3,4],
                    edges = fromList [(Edge 1 3), (Edge 2 3), (Edge 3 4)]
                    }

-- g_subArvore2
-- Grafo correspondente a uma árvore. Sub-grafo de g_floresta.
--         5 --> 6
g_subArvore2 :: Graph Int
g_subArvore2 = Graph {
                        nodes = fromList [5,6],
                        edges = fromList [(Edge 5 6)]
                        }

-- g_ciclo
-- Grafo com um ciclo.
--     1 ---> 2
--      ^    /
--       \  v
--         3 ----> 4
g_ciclo :: Graph Int
g_ciclo = Graph {
                nodes = fromList [1,2,3,4],
                edges = fromList [(Edge 1 2), (Edge 2 3), (Edge 3 1), (Edge 3 4)]
                }

-- g_cicloTransposto
-- O transposto do grafo anterior
--     1 <--- 2
--      \    ^
--       v  /
--         3 <---- 4
g_cicloTransposto :: Graph Int
g_cicloTransposto = Graph {
                            nodes = fromList [1,2,3,4],
                            edges = fromList [(Edge 1 3), (Edge 2 1), (Edge 3 2), (Edge 4 3)]
                            }

-- grafo invalido por ter nas arestas referência a um vértice invalido
g_invalido :: Graph Int
g_invalido = Graph {
                    nodes = fromList [1,2],
                    edges = fromList [(Edge 1 6)]
                    }

g_invalido2 :: Graph Int
g_invalido2 = Graph {
                    nodes = fromList [ ],
                    edges = fromList [(Edge 1 6), (Edge 6 2)]
                    }

-- grafo correspondente à arvore gerada pela travessia do grafo g_subArvore a partir do vertice 1
g_trav1 :: Graph Int
g_trav1 = Graph {
                    nodes = fromList [1,3,4],
                    edges = fromList [(Edge 3 1), (Edge 4 3)]
                    }

g_vazio :: Graph Int
g_vazio = Graph.empty

g_bft2 :: Graph Int
g_bft2 = Graph {nodes = fromList [1,2,3,4], 
                edges = fromList [Edge {source = 3, target = 1},Edge {source = 4, target = 3}]
                }

g_nodosSep :: Graph Int
g_nodosSep = Graph {
                    nodes = fromList [1,3,4],
                    edges = fromList []
                    }

-- TESTES FUNCOES --

--Teste swap
t_swap :: Test
t_swap = swap (Edge 1 2) ~?= (Edge 2 1)

--Teste isEmpty
t_isEmpty1 :: Test
t_isEmpty1 = Graph.isEmpty g_vazio ~?= True

t_isEmpty2 :: Test
t_isEmpty2 = Graph.isEmpty g_floresta ~?= False

--Teste isValid
t_isValid1 :: Test
t_isValid1 = Graph.isValid g_invalido ~?= False

t_isValid2 :: Test
t_isValid2 = Graph.isValid g_ciclo ~?= True

t_isValid3 :: Test
t_isValid3 = Graph.isValid g_vazio ~?= True

--Teste isDAG
t_isDAG1 :: Test
t_isDAG1 = isDAG g_ciclo ~?= False

t_isDAG2 :: Test
t_isDAG2 = isDAG g_floresta ~?= True

--Teste isForest
t_isForest1 :: Test
t_isForest1 = isForest g_floresta ~?= True

t_isForest2 :: Test
t_isForest2 = isForest g_ciclo ~?= False

--Teste t_isSubGraphOf
t_isSubGraphOf1 :: Test
t_isSubGraphOf1 = isSubgraphOf g_subArvore g_floresta ~?= True

t_isSubGraphOf2 :: Test
t_isSubGraphOf2 = isSubgraphOf g_ciclo g_floresta ~?= False

--Teste adj
t_adj :: Test
t_adj = Graph.adj g_ciclo 3 ~?= Set.fromList [(Edge 3 1),(Edge 3 4)]

--Teste transpose
t_transpose :: Test
t_transpose = Graph.transpose g_ciclo ~?= g_cicloTransposto

--Teste union
t_union :: Test
t_union = Graph.union g_subArvore g_subArvore2 ~?= g_floresta

--Teste bft
t_bft1 :: Test
t_bft1 = bft g_floresta (Set.fromList [1]) ~?= g_trav1

t_bft2 :: Test
t_bft2 = bft g_floresta Set.empty ~?= Graph.empty

t_bft3 :: Test
t_bft3 = bft Graph.empty (Set.fromList [1,2]) ~?= Graph {nodes = fromList [1,2], edges = fromList []}

--Teste reachable
t_reachable :: Test
t_reachable = reachable g_floresta 1 ~?= (Set.fromList [1,3,4])

--Teste t_isPathOf
t_isPathOf1 :: Test
t_isPathOf1 = isPathOf [(Edge 1 3), (Edge 3 4)] g_floresta ~?= True

t_isPathOf2 :: Test
t_isPathOf2 = isPathOf [(Edge 1 5)] g_floresta ~?= False

t_isPathOf3 :: Test
t_isPathOf3 = isPathOf ([]) g_floresta ~?= True

--Teste path
t_path1 :: Test
t_path1 = Graph.path g_floresta 1 5 ~?= Nothing

t_path2 :: Test
t_path2 = Graph.path g_ciclo 3 2 ~?= Just [(Edge 3 1),(Edge 1 2)]

--Teste topo
t_topo :: Test
t_topo = Graph.topo g_floresta ~?= [(Set.fromList [1,2,5]),
                                    (Set.fromList [3, 6]),
                                    (Set.fromList [4])
                                    ]

--Teste igualdade Edges
t_EqEdge1 :: Test
t_EqEdge1 = (Edge 1 2) == (Edge 1 2) ~?= True

t_EqEdge2 :: Test
t_EqEdge2 = (Edge 2 1) /= (Edge 1 2) ~?= True

--Testes show
t_showEdge :: Test
t_showEdge = show (Edge 1 2) ~?= "Edge {source = 1, target = 2}"

t_showGraph1 :: Test
t_showGraph1 = show g_subArvore2 ~?= "Graph {nodes = fromList [5,6], edges = fromList [Edge {source = 5, target = 6}]}"

t_showGraph2 :: Test
t_showGraph2 = show g_vazio ~?= "Graph {nodes = fromList [], edges = fromList []}"

t_showGraph3 :: Test
t_showGraph3 = show g_nodosSep ~?= "Graph {nodes = fromList [1,3,4], edges = fromList []}"

t_showGraph4 :: Test
t_showGraph4 = show g_floresta ~?= "Graph {nodes = fromList [1,2,3,4,5,6], edges = fromList [Edge {source = 1, target = 3},Edge {source = 2, target = 3},Edge {source = 3, target = 4},Edge {source = 5, target = 6}]}"


--
-- Teste aleatório
--

--
-- Tarefa 2
--
-- A instância de Arbitrary para grafos definida abaixo gera grafos
-- com muito poucas arestas, como se pode constatar testando a
-- propriedade prop_valid.
-- Defina uma instância de Arbitrary menos enviesada.
-- Este problema ainda é mais grave nos geradores dag e forest que
-- têm como objectivo gerar, respectivamente, grafos que satisfazem
-- os predicados isDag e isForest. Estes geradores serão necessários
-- para testar propriedades sobre estas classes de grafos.
-- Melhore a implementação destes geradores por forma a serem menos enviesados.
--

-- Instância de Arbitrary para arestas
instance Arbitrary v => Arbitrary (Edge v) where
    arbitrary = do s <- arbitrary
                   t <- arbitrary
                   return $ Edge {source = s, target = t}

-- Gerador de grafos. Gera primeiro nodos e constroi grafos com esses nodos
-- para garantir que o grafo é válido por construção
instance (Ord v, Arbitrary v) => Arbitrary (Graph v) where
    arbitrary = aux `suchThat` isValid
        where aux = do kns <- choose(0, 50)
                       ns  <- sequence (replicate kns arbitrary)
                       kes <- choose(0, 30)
                       es  <- edgeFromNodes kes ns
                       return $ Graph {nodes = fromList ns, edges = fromList es}

--Função auxiliar ao gerador que gera n nodos a partir de uma lista de pontos dada.
edgeFromNodes :: Int -> [a] -> Gen [(Edge a)]
edgeFromNodes _ [] = return []
edgeFromNodes 0 _ = return []
edgeFromNodes n l@(x:xs) = do origem <- elements l
                              destino <- elements l
                              mais <- edgeFromNodes (n-1) l
                              return $ ((Edge origem destino):mais)

--Propriedade para testar gerador de grafos
prop_valid :: Graph Int -> Property
prop_valid g = collect (length (edges g)) $ isValid g

--Gerador de DAG's original. A função dag2 corresponde a um gerador melhorado
dag :: (Ord v, Arbitrary v) => Gen (DAG v)
dag = arbitrary `suchThat` isDAG

--Propriedade para testar gerador de DAG's original
prop_dag :: Property
prop_dag = forAll (dag :: Gen (DAG Int)) $ \g -> collect (length (edges g)) $ isDAG g

--Gerador de dags melhorado. Gera grafo aleatório e torna-o um DAG
dag2 :: (Ord v, Arbitrary v) => Gen (DAG v)
dag2 =  do 
         g <- arbitrary
         return (dagify g)

--Propriedade para testar gerador de DAG melhorado
prop_dag2 :: Property
prop_dag2 = forAll (dag2 :: Gen (DAG Int)) $ \g -> collect (length (edges g)) $ isDAG g

--Função que transforma um grafo num DAG
dagify :: (Ord v) => Graph v -> DAG v
dagify g    | isDAG g   = g
            | otherwise = Graph{nodes = nodes g, edges = edgesSC2 g}
            
--Primeira tentativa de função que para um grafo devolve um set de arestas sem ciclos.
--Retira demasiados nodos. edgesSC2 representa uma versão melhorada desta função
edgesSC1 :: (Ord v) => Graph v -> Set (Edge v)
edgesSC1 g = Set.filter (\e -> not (Set.member (source e) (reachable g (target e)))) (edges g) 

--Função que para um grafo devolve um set de arestas sem ciclos
edgesSC2 :: (Ord v) => Graph v -> Set (Edge v)
edgesSC2 g = edgesSC2_aux g edgesOriginais
                where edgesOriginais = Set.elems (edges g)

--Função auxiliar à anterior que itera sobre a lista de Edges de um grafo
--e para cada um deles decide se o retira do grafo.
edgesSC2_aux :: (Ord v) => Graph v -> [Edge v] -> Set (Edge v)
edgesSC2_aux g []        = edges g
edgesSC2_aux g l@(x:xs)  = if Set.member origem (reachable g destino)
                            then edgesSC2_aux (eliminaEdgeGrafo g (Edge origem destino)) xs
                            else edgesSC2_aux g xs
                            where
                                origem = source x
                                destino = target x

--Elimina uma aresta de um grafo
eliminaEdgeGrafo :: (Ord v) => Graph v -> Edge v -> Graph v
eliminaEdgeGrafo g e = Graph {
                                nodes = nodes g,
                                edges = delete e (edges g)
                             };



-- Gerador de florestas

--Gerador de florestas original. A função forest2 representa um gerador de florestas melhorado
forest :: (Ord v, Arbitrary v) => Gen (Forest v)
forest = arbitrary `suchThat` isForest

--Propriedade para testar gerador de grafos original
prop_forest :: Property
prop_forest = forAll (forest :: Gen (Forest Int)) $ \g -> collect (length (edges g)) $ isForest g

--Gerador de florestas melhorado. Gera dag's (usando o gerador de dag's melhorado) e transforma
--o resultado numa floresta.
forest2 :: (Ord v, Arbitrary v) => Gen (Forest v)
forest2 = do 
            g <- dag2
            return (forestify g)

--Propriedade para testar gerador de florestas melhorado
prop_forest2 :: Property
prop_forest2 = forAll (forest2 :: Gen (Forest Int)) $ \g -> collect (length (edges g)) $ isForest g

--Transforma grafo em floresta
forestify :: (Ord v) => Graph v -> Forest v
forestify g     | isForest g    = g
                | otherwise     = Graph{nodes = nodes g, edges = edgesForest g}

--Dado um grafo devolve um set de edges que correspondem a uma floresta
edgesForest :: (Ord v) => Graph v -> Set (Edge v)
edgesForest g = edgesForest_aux g leo Set.empty
                    where leo = Set.elems (nodes g)

--Função auxiliar ao transformador de grafos em florestas.
--Recebe um grafo e uma lista de nodos e vai construindo o resultado num acumulador acc
edgesForest_aux :: (Ord v) => Graph v -> [v] -> Set (Edge v) -> Set (Edge v)
edgesForest_aux g [] acc       = acc
edgesForest_aux g l@(n:ns) acc = if (Set.size adjacentes > 0 && (source candidato /= target candidato))
                                    then edgesForest_aux g ns (Set.insert candidato acc)
                                    else edgesForest_aux g ns acc
                                    where
                                        adjacentes = adj g n
                                        candidato  = Set.elemAt 0 adjacentes



--
-- Tarefa 3
--
-- Defina propriedades QuickCheck para testar todas as funcoes
-- do módulo Graph.
--

--swap :: Edge v -> Edge v
--Testa se fazer um swap duas vezes a uma aresta resulta na aresta original
prop_swap :: Property
prop_swap = forAll (arbitrary :: Gen (Edge Int)) (\g -> swap (swap g) == g)

--empty :: Graph v
--Gerador de grafos vazios
geraGVazios :: Gen (Graph a)
geraGVazios = return Graph.empty

--Testa se um grafo vazio não tem nodos nem arestas.
prop_empty :: Property
prop_empty = forAll (geraGVazios :: Gen (Graph Int)) (\g -> Set.size (nodes g) ==0 && Set.size (edges g) ==0)

--isEmpty :: Graph v -> Bool
--Testa se isEmpty devolve o valor esperado para grafos vazios.
prop_isEmpty :: Property
prop_isEmpty = forAll (geraGVazios :: Gen (Graph Int)) $ \g -> isEmpty g

--isValid :: Ord v => Graph v -> Bool
--Testa se isValid devolve o valor esperado para grafos gerados aleatoriamente (que são válidos)
prop_isValid :: Property
prop_isValid = forAll (arbitrary :: Gen (Graph Int)) $ \g -> collect (length (edges g)) $ isValid g

--isDAG :: Ord v => Graph v -> Bool
--Testa se isDAG devolve o valor esperado para DAG's gerados aleatoriamente
prop_isDAG :: Property
prop_isDAG = forAll (dag2 :: Gen (DAG Int)) $ \g -> collect (length (edges g)) $ isDAG g

--isForest :: Ord v => DAG v -> Bool
--Testa se isForest devolve o valor esperado para florestas geradas aleatoriamente
prop_isForest :: Property
prop_isForest = forAll (forest2 :: Gen (Forest Int)) $ \g -> collect (length (edges g)) $ isForest g

--isSubgraphOf :: Ord v => Graph v -> Graph v -> Bool
--Testa se um subgrafo é subgrafo dele proprio
prop_isSubgraphOf :: Property
prop_isSubgraphOf = forAll (dag :: Gen (Graph Int)) $ \g -> collect (length (edges g)) $ isSubgraphOf g

--adj :: Ord v => Graph v -> v -> Set (Edge v)        
prop_adj :: Graph Int -> Property
prop_adj g = (Set.size (nodes g)>0) ==> forAll (elements $ elems $ nodes g) $ \v -> adj g v `isSubsetOf` edges g

--transpose :: Ord v => Graph v -> Graph v
--Testa se o resultado de fazer transpose duas vezes a um grafo corresponde ao grafo original
prop_transpose :: Property
prop_transpose = forAll (arbitrary :: Gen (Graph Int)) (\g -> transpose (transpose g) == g)

--union :: Ord v => Graph v -> Graph v -> Graph v.
--Testa se o resultado da uniao de dois grafos é um grafo com numero maior ou igual de arestas e nodos do
--grafo original
prop_union :: Graph Int -> Property
prop_union g1 = forAll (arbitrary :: Gen (Graph Int)) (\g2 -> (Set.size (nodes (g1 `Graph.union` g2)))>=max (Set.size (nodes g1)) (Set.size (nodes g2)) 
                                                           && (Set.size (edges (g1 `Graph.union` g2)))>=max (Set.size (edges g1)) (Set.size (edges g1)))

--bft :: Ord v => Graph v -> Set v -> Forest v
--Testa se a função bft devolve arestas
prop_bft :: Set Int -> Property
prop_bft s = forAll (arbitrary :: Gen (Graph Int)) (\g -> isForest (bft g s))

--path :: Ord v => Graph v -> v -> v -> Maybe (Path v)
--reachable :: Ord v => Graph v -> v -> Set v
--Se há um caminho entre a e b, então b tem que ser alcancavel a partir de a
prop_reachable :: (Ord v) => Graph v -> v -> v -> Property
prop_reachable g a b = isJust (path g a b) ==> collect (isJust (path g a b)) $ Set.member b (reachable g a)

--isPathOf :: Ord v => Path v -> Graph v -> Bool
--Se p é caminho do grafo g, com g não vazio, então a função path deverá devolver Just _
prop_isPathOf :: (Ord v) => Graph v -> Graph.Path v -> Property
prop_isPathOf g p = (length p>0 && (isPathOf p g)) ==> collect (isPathOf p g) $ isJust (path g (source (head p)) (source (last p)))

--topo :: Ord v => DAG v -> [Set v]
--O resultado da funcao topo deve manter o mesmo conjunto de nodos do grafo
prop_topo :: Property
prop_topo = forAll (dag2 :: Gen (Graph Int)) (\g -> unions (topo g) == nodes g)
